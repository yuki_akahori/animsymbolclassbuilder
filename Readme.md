# AnimatorControllerから定数シンボル情報を作成するユーティリティ

AnimatorのPlay呼び出しを

```csharp
Animator anm;
anm.Play("hoge", 1);
```

ではなく

```csharp
Animator anm;
anm.Play(EnemyAnmSymbol.State.Base.Hoge, EnemyAnmSymbol.Layer.Base);
```

というように定数シンボルでやりたいため作成。

## 定数シンボルでやりたい理由
- 文字だとタイプミスが起きる可能性がある
- 文字だと毎回Animator.StringToHashが実行される

# 使い方

AnimSymbolClassBuilderでアニメーターから定数シンボルクラスのソースコードが作れる。  
それをプロジェクトの都合の良いように使う。

サンプルとしてアニメーターを選択した状態で右クリックでシンボルクラスファイルを作るエディタ拡張を用意した。
SymbolToolSample.cs

# 参考

[【Unity】Animator のステート名を定数で管理するクラスを生成するエディタ拡張](http://baba-s.hatenablog.com/entry/2015/05/06/120810)

