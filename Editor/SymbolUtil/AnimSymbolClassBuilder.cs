using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEditor.Animations;

namespace SymbolUtil
{
    /// <summary>
    /// アニメーターの以下の情報を持つ定数クラスのC#ソースコードを生成するクラス
    /// ・ステートハッシュ
    /// ・パラメータハッシュ
    /// ・レイヤーID
    /// 
    /// １ファイルに複数のアニメーターのシンボルを出力できるようにしている。
    /// また、シンボル用にネームスペースを切るケースも多いと思うので、ネームスペースも作成できるようにしている。
    /// 
    /// 参考
    /// http://baba-s.hatenablog.com/entry/2015/05/06/120810
    /// </summary>
    public class AnimSymbolClassBuilder
    {

        /// <summary>
        /// コンストラクタ
        /// 
        /// splitStateClassはステートマシンごとにクラスを切るかどうかのオプションです。
        /// 異なるステートマシンで同名のステート名がある場合にtrueにするとステートごとにクラスが切られるのでコンパイルエラーになりません。
        /// ステート名を含めるとどうしてもシンボル名が長くなるのでデフォルトはfalseです
        /// </summary>
        /// <param name="stringBuilder">ソースコード文字列書き込み先</param>
        /// <param name="splitStateClass">ステートごとにクラスを切るか</param>
        /// <param name="indentFormat">インデントのフォーマット　デフォルトはスペース4 つ</param>
        public AnimSymbolClassBuilder(System.Text.StringBuilder stringBuilder, bool splitStateClass = false, string indentFormat = "    ")
        {
            _indentFormat = indentFormat;
            _str = stringBuilder;
            _nameSpace = new Queue<string>();
            _splitStateClass = splitStateClass;
            UpdateIndentStr();
        }

        /// <summary>
        /// ネームスペースを追加します
        /// </summary>
        /// <param name="namespaceName">ネームスペース名</param>
        public void PushNamespace(string namespaceName)
        {
            Debug.AssertFormat(!_nameSpace.Contains(namespaceName), "ネームスペース名が重複しています {0}", namespaceName);

            _str.AppendLine(string.Format("{0}namespace {1}", _indentStr, namespaceName));
            _str.AppendLine(_indentStr + "{");
            _nameSpace.Enqueue(namespaceName);

            UpdateIndentStr();
        }

        /// <summary>
        /// 最も新しく追加されたネームスペースを取り除きます
        /// </summary>
        public void PopNamespace()
        {
            Debug.Assert(_nameSpace.Count > 0, "ネームスペースが設定されていません");
            _nameSpace.Dequeue();
            UpdateIndentStr();
            _str.AppendLine(_indentStr + "}");
        }

        /// <summary>
        /// アニメーターの定数情報を追加します
        /// </summary>
        /// <param name="animatorController">アニメータコントローラ</param>
        /// <param name="className">定数クラス名</param>
        /// <param name="classComment">クラスコメント。不要であればnullを渡してください</param>
        public void AddAnimator(AnimatorController animatorController, string className, string classComment = null)
        {
            if (classComment != null)
            {
                _str.AppendLine(string.Format("{0}{1}", _indentStr, classComment));
            }

            _str.AppendLine(string.Format("{0}public static class {1}", _indentStr, className));
            _str.AppendLine(_indentStr + "{");

            var innerClassStartIndent = _indentStr + _indentFormat;

            // ステート定数の作成
            {
                _str.AppendLine(innerClassStartIndent + "public static class State");
                _str.AppendLine(innerClassStartIndent + "{");
                var stateMachineInfo = new StateMachineInfo(animatorController);
                var stateMachineClassIndent = innerClassStartIndent + _indentFormat;
                foreach (var stateMachine in stateMachineInfo.stateMachineInfos)
                {
                    _str.AppendLine(StateMachineInfo.BuildStateHashClassCode(stateMachine, stateMachineClassIndent, _indentFormat, _splitStateClass));
                }
                _str.AppendLine(innerClassStartIndent + "}");
            }

            // パラメータ定数の作成
            _str.AppendLine();
            {
                _str.AppendLine(innerClassStartIndent + "public static class Param");
                _str.AppendLine(innerClassStartIndent + "{");
                var memberIndent = innerClassStartIndent + _indentFormat;
                foreach (var param in animatorController.parameters)
                {
                    var memberName = NameUtil.RemoveInvalidChars(param.name);
                    _str.AppendLine(string.Format("{0}public const int {1} = {2};", memberIndent, memberName, param.nameHash.ToString()));
                }
                _str.AppendLine(innerClassStartIndent + "}");
            }

            // レイヤー定数の作成
            _str.AppendLine();
            {
                _str.AppendLine(innerClassStartIndent + "public static class Layer");
                _str.AppendLine(innerClassStartIndent + "{");
                var memberIndent = innerClassStartIndent + _indentFormat;

                for (var i = 0; i < animatorController.layers.Length; ++i)
                {
                    var layerName = NameUtil.RemoveInvalidChars(animatorController.layers[i].name);
                    _str.AppendLine(string.Format("{0}public const int {1} = {2};", memberIndent, layerName, i.ToString()));
                }

                _str.AppendLine(innerClassStartIndent + "}");
            }

            _str.AppendLine(_indentStr + "}");
        }

        /// <summary>
        /// 現在のネームスペース階層の情報でインデント用文字列を更新します。
        /// </summary>
        void UpdateIndentStr()
        {
            _indentStr = string.Empty;
            for (var i = 0; i < _nameSpace.Count; ++i)
            {
                _indentStr += _indentFormat;
            }
        }

        /// <summary>
        /// 1階層あたりのインデント
        /// </summary>
        string _indentFormat;
        System.Text.StringBuilder _str;
        string _indentStr;
        bool _splitStateClass;
        Queue<string> _nameSpace;
    }

    /// <summary>
    /// アニメーター内に含まれるステートマシン情報
    /// </summary>
    class StateMachineInfo
    {
        public StateMachineInfo(AnimatorController controller)
        {
            _stateMachineInfos = new List<AnimatorStateMachineInfo>();
            foreach (var layer in controller.layers)
            {
                Collect(layer.name, layer.stateMachine, isSubStateMachine: false);
            }
        }

        /// <summary>
        /// アニメーションステートの収集処理
        /// </summary>
        void Collect(string layerName, AnimatorStateMachine stateMachine, bool isSubStateMachine)
        {
            _stateMachineInfos.Add(new AnimatorStateMachineInfo(stateMachine, layerName, isSubStateMachine));
            foreach (var subStateMachine in stateMachine.stateMachines)
            {
                // サブステートマシンは入れ子構造が可能。末端まで再帰呼び出しする。
                Collect(layerName, subStateMachine.stateMachine, isSubStateMachine: true);
            }
        }

        /// <summary>
        /// アニメーションステートのステートハッシュクラスのソースコードを生成する
        /// public static ステートマシン名
        /// {
        ///     public const int ステート名 = ステートハッシュ;
        ///     ...    
        /// }
        /// というフォーマットで生成する。
        /// ※サブステートマシンは処理しない
        /// </summary>
        /// <param name="stateMachine">ステートマシン</param>
        /// <param name="classStartIndent">クラス作成開始位置のインデント</param>
        /// <param name="indentFormat">インデントのフォーマット</param>
        /// <param name="splitStateClass">ステートマシン名で暮らすを切るか</param>
        /// <returns>生成されたソースコートテキスト</returns>
        public static string BuildStateHashClassCode(AnimatorStateMachineInfo stateMachineInfo, string classStartIndent, string indentFormat, bool splitStateClass)
        {
            var str = new System.Text.StringBuilder();

            var memberStartIndent = classStartIndent;

            if (splitStateClass)
            {
                str.AppendLine(string.Format("{0}public static class {1}", classStartIndent, NameUtil.RemoveInvalidChars(stateMachineInfo.stateMachine.name)));
                str.AppendLine(classStartIndent + "{");
                memberStartIndent += indentFormat;
            }

            string layerText = stateMachineInfo.isSubStateMachine ? stateMachineInfo.layerName + "." : "";
            foreach (var state in stateMachineInfo.stateMachine.states)
            {
                var memberName = NameUtil.RemoveInvalidChars(state.state.name);
                var hash = Animator.StringToHash(layerText + stateMachineInfo.stateMachine.name + "." + state.state.name);
                str.AppendLine(string.Format("{0}public const int {1} = {2};", memberStartIndent, memberName, hash.ToString()));
            }

            if (splitStateClass)
            {
                str.AppendLine(classStartIndent + "}");
            }

            return str.ToString();
        }

        public class AnimatorStateMachineInfo
        {
            public AnimatorStateMachineInfo(AnimatorStateMachine stateMachine, string layerName, bool isSubStateMachine)
            {
                this.stateMachine = stateMachine;
                this.layerName = layerName;
                this.isSubStateMachine = isSubStateMachine;
            }
            public AnimatorStateMachine stateMachine;
            public string layerName;  // サブステートマシーンのfullPathHash用。　レイヤー名.サブステートマシン名.ステート名 がハッシュ
            public bool isSubStateMachine;
        }

        // SubStateMachineの場合 Layer情報が必要だった

        public List<AnimatorStateMachineInfo> stateMachineInfos { get { return _stateMachineInfos; } }

        List<AnimatorStateMachineInfo> _stateMachineInfos;
    }

}
