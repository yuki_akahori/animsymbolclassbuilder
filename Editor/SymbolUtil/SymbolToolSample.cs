﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

namespace SymbolUtil
{
    public class SymbolToolSample
    {
        const string _SymboleFileFolderPath = "Assets/Symbol";

        /// <summary>
        /// AnimationControllerを選択してを右クリックメニューからシンボルを作成するサンプル
        /// Assets/SymbolフォルダにSymbol_アニメーターコントローラー名でシンボルクラスのファイルが作られる
        /// ネームスペースはSymbol.Animとなる
        /// 
        /// 定数クラスのソースコード作成処理はAnimSymbolClassBuilderに分離されているので各プロジェクトで都合の良いように使ってもらいたい。
        /// </summary>
        [MenuItem("Assets/AnimationSymbol/ファイル名からシンボルを作成")]
        static void CreateFromFileName()
        {
            if (!System.IO.Directory.Exists(_SymboleFileFolderPath))
            {
                System.IO.Directory.CreateDirectory(_SymboleFileFolderPath);
            }

            var outputFiles = new List<string>();
            var strBuilder = new System.Text.StringBuilder();
            foreach (var controller in GetSelectedAnimators())
            {
                strBuilder.Length = 0;

                // ファイルヘッダ
                strBuilder.AppendFormat("// アニメーターコントローラー {0} のシンボル情報です。\n", controller.name);
                strBuilder.Append("// このクラスは自動的に生成されました。手動で編集しないでください。\n");
                strBuilder.AppendLine();

                var builder = new SymbolUtil.AnimSymbolClassBuilder(strBuilder);

                // シンボル用にネームスペースを切る
                builder.PushNamespace("Symbol");
                builder.PushNamespace("Anim");

                // シンボルクラス名はアニメーターコントローラー名と合わせる
                var className = SymbolUtil.NameUtil.RemoveInvalidChars(controller.name);
                builder.AddAnimator(controller, className);

                builder.PopNamespace();
                builder.PopNamespace();

                // アニメーターコントローラー名からシンボルファイル名を決める
                var path = System.IO.Path.Combine(_SymboleFileFolderPath, "Symbol" + NameUtil.RemoveInvalidChars(controller.name) + ".cs");
                System.IO.File.WriteAllText(path, strBuilder.ToString(), System.Text.Encoding.UTF8);

                AssetDatabase.ImportAsset(path);
                outputFiles.Add(path);
            }

            AssetDatabase.Refresh();

            // 処理完了ダイアログを出す
            strBuilder.Length = 0;
            foreach (var outputFile in outputFiles)
            {
                strBuilder.Append(outputFile + "\n");
            }

            EditorUtility.DisplayDialog("以下のシンボルファイルを作成しました", strBuilder.ToString(), "OK");
        }

        [MenuItem("Assets/AnimationSymbol/ファイル名からシンボルを作成", isValidateFunction: true)]
        static bool ValidateCreateFromFileName()
        {
            return GetSelectedAnimators().Length > 0;
        }

        /// <summary>
        /// 現在プロジェクトウィンドウで選択中のアニメーションコントローラ配列を取得する
        /// </summary>
        static UnityEditor.Animations.AnimatorController[] GetSelectedAnimators()
        {
            return Selection.GetFiltered<UnityEditor.Animations.AnimatorController>(SelectionMode.Assets);
        }
    }

}