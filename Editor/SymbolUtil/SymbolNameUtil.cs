﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SymbolUtil
{
    static public class NameUtil
    {
        // 無効な文字を管理する配列
        private static readonly string[] _InvalidChars =
        {
        " ", "!", "\"", "#", "$",
        "%", "&", "\'", "(", ")",
        "-", "=", "^",  "~", "\\",
        "|", "[", "{",  "@", "`",
        "]", "}", ":",  "*", ";",
        "+", "/", "?",  ".", ">",
        ",", "<"
        };

        /// <summary>
        /// 無効な文字を削除します
		/// 
		/// [補足]
		/// 以下で紹介されていた処理です
        /// http://baba-s.hatenablog.com/entry/2015/05/06/120810
        /// </summary>
        public static string RemoveInvalidChars(string str)
        {
            System.Array.ForEach(_InvalidChars, c => str = str.Replace(c, string.Empty));
            return str;
        }
    }
}
